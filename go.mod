module example.com/m

go 1.19

require (
	go.etcd.io/bbolt v1.3.7
	oras.land/oras-go/v2 v2.2.1
)

require (
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.1.0-rc4 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
)

replace github.com/spdx/tools-golang => github.com/spdx/tools-golang v0.3.0
