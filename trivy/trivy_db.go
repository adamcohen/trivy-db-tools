package trivy

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	bolt "go.etcd.io/bbolt"
	"oras.land/oras-go/v2"
	"oras.land/oras-go/v2/content/file"
	"oras.land/oras-go/v2/registry/remote"
)

type TrivyDB struct {
	DB *bolt.DB
}

type Actions interface {
	GetAdvisoryMetadata(distro []byte, pkg []byte, advID []byte) ([]byte, error)
	GetVulnerability(advID []byte) ([]byte, error)
	Close() error
}

func (t *TrivyDB) Close() error {
	return t.DB.Close()
}

func (t *TrivyDB) GetAdvisoryMetadata(distro []byte, pkg []byte, advID []byte) ([]byte, error) {
	var value []byte

	err := t.DB.View(func(tx *bolt.Tx) error {
		if len(distro) == 0 || len(pkg) == 0 || len(advID) == 0 {
			return errors.New("distro or pkg or adv id bucket was empty")
		}

		bkt := tx.Bucket(distro)
		if bkt == nil {
			return nil
		}

		pkgBucket := bkt.Bucket(pkg)
		if pkgBucket == nil {
			return nil
		}

		value = pkgBucket.Get(advID)
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get data from db: %w", err)
	}
	return value, nil

}

func (t *TrivyDB) GetVulnerability(advID []byte) ([]byte, error) {
	var value []byte

	err := t.DB.View(func(tx *bolt.Tx) error {
		if len(advID) == 0 {
			return errors.New("adv id bucket was empty")
		}

		bkt := tx.Bucket([]byte("vulnerability"))
		if bkt == nil {
			return nil
		}

		value = bkt.Get(advID)
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get data from db: %w", err)
	}
	return value, nil
}

func NewDB(tag string) (*TrivyDB, error) {
	reg := "registry.gitlab.com/gitlab-org/security-products/dependencies"
	repo, err := remote.NewRepository(reg + "/trivy-db-glad")
	if err != nil {
		return nil, err
	}

	err = getTrivyFiles(tag, repo)
	if err != nil {
		return nil, err
	}

	db, err := bolt.Open(fmt.Sprintf("./%s.db", tag), 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, err
	}

	return &TrivyDB{
		DB: db,
	}, nil
}

func getTrivyFiles(tag string, repo *remote.Repository) error {
	fs, err := file.New("./")
	if err != nil {
		return err
	}
	defer fs.Close()

	err = download(tag, repo, fs)
	if err != nil {
		return err
	}

	if err := extractTarGz("./db.tar.gz", "./"); err != nil {
		fmt.Printf("Error: %v\n", err)
		return err
	}

	// 3. Rename db file with the correct tag
	if err := os.Rename("./trivy.db", fmt.Sprintf("%s.db", tag)); err != nil {
		fmt.Printf("Error renaming file: %v\n", err)
		return err
	}
	return nil
}

func extractTarGz(tarGzFilePath, extractDir string) error {
	file, err := os.Open(tarGzFilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	gzipReader, err := gzip.NewReader(file)
	if err != nil {
		return err
	}
	defer gzipReader.Close()

	tarReader := tar.NewReader(gzipReader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		targetPath := filepath.Join(extractDir, header.Name)

		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.MkdirAll(targetPath, os.ModePerm); err != nil {
				return err
			}
		case tar.TypeReg, tar.TypeRegA:
			file, err := os.Create(targetPath)
			if err != nil {
				return err
			}
			defer file.Close()
			if _, err := io.Copy(file, tarReader); err != nil {
				return err
			}
		}
		fmt.Printf("Extracted: %s\n", targetPath)
	}

	fmt.Println("Extraction complete.")
	return nil
}

func download(tag string, repo *remote.Repository, fs *file.Store) error {
	manifestDescriptor, err := oras.Copy(context.Background(), repo, tag, fs, tag, oras.DefaultCopyOptions)
	fmt.Println("manifest descriptor:", manifestDescriptor)
	return err
}
