package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"strings"

	"example.com/m/trivy"
	bolt "go.etcd.io/bbolt"
)

var (
	oldTag = "2023082703"
	newTag = "2023082803"
)

func compareTrivyDBs() error {

	oldTrivy, err := trivy.NewDB(oldTag)
	if err != nil {
		log.Fatal(err)
	}

	latestTrivy, err := trivy.NewDB(newTag)
	if err != nil {
		log.Fatal(err)
	}

	// Compare distros ! Not vulnerabilities
	if err = latestTrivy.DB.View(func(tx *bolt.Tx) error {
		return tx.ForEach(func(distro []byte, b *bolt.Bucket) error {

			// For each Distro
			if skip := skipDistro(distro, b); skip {
				return nil
			}

			distroCursor := b.Cursor()
			diffEditCounter := 0
			diffAdditionCounter := 0
			diffDeleteionCounter := 0
			for packageName, _ := distroCursor.First(); packageName != nil; packageName, _ = distroCursor.Next() {

				pkgCursor := b.Bucket(packageName).Cursor()
				for advID, advMetadata := pkgCursor.First(); advID != nil; advID, advMetadata = pkgCursor.Next() {
					oldAdvMetadata, err := oldTrivy.GetAdvisoryMetadata(distro, packageName, advID)
					if err != nil {
						fmt.Println(err)
						log.Fatal(err)
					}
					if !bytes.Equal(oldAdvMetadata, advMetadata) {
						if oldAdvMetadata == nil {
							diffAdditionCounter++
						} else if advMetadata == nil {
							diffDeleteionCounter++
						} else {
							diffEditCounter++
						}
					}
				}

			}
			if diffEditCounter != 0 {
				fmt.Printf("For %s we found %d edit %d additions %d deletions \n", string(distro), diffEditCounter, diffAdditionCounter, diffDeleteionCounter)
			}

			return nil
		})
	}); err != nil {
		log.Fatal(err)
	}

	// Compare Vulnerabilities
	if err = latestTrivy.DB.View(func(tx *bolt.Tx) error {
		vulnBucket := tx.Bucket([]byte("vulnerability"))
		if vulnBucket == nil {
			return errors.New("vulnerability bucket was not found")
		}

		c := vulnBucket.Cursor()
		diffvEditCounter := 0
		diffvAdditionCounter := 0
		diffvDeleteionCounter := 0
		for advID, advMetadata := c.First(); advID != nil; advID, advMetadata = c.Next() {
			oldAdvMetadata, err := oldTrivy.GetVulnerability(advID)
			if err != nil {
				fmt.Println(err)
				log.Fatal(err)
			}
			if !bytes.Equal(oldAdvMetadata, advMetadata) {
				if oldAdvMetadata == nil {
					diffvAdditionCounter++
				} else if advMetadata == nil {
					diffvDeleteionCounter++
				} else {
					diffvEditCounter++
				}
			}
		}
		fmt.Printf("Vulnerabilities: %d edit %d additions %d deletions \n", diffvEditCounter, diffvAdditionCounter, diffvDeleteionCounter)
		return nil
	}); err != nil {
		log.Fatal(err)
	}

	return nil
}

func skipDistro(distro []byte, b *bolt.Bucket) bool {
	if string(distro) == "vulnerability" ||
		string(distro) == "data-source" ||
		string(distro) == "Red Hat CPE" ||
		string(distro) == "Red Hat" ||
		strings.Contains(string(distro), "::") {
		// fmt.Println("Skipping " + string(distro))
		return true
	}

	if b == nil {
		fmt.Println("Skipping empty distro bucket")
		return true
	}

	return false
}

func main() {
	if err := compareTrivyDBs(); err != nil {
		panic(err)
	}
}
