# trivy-db-tools

## compare-dbs

### Manually downloading a trivy db

```shell
$ oras pull registry.gitlab.com/adamcohen/trivy-db-tools/sample-dbs:latest

$ tar -xzf trivy-db/db.tar.gz -C trivy-db && tar -xzf trivy-db-2/db.tar.gz -C trivy-db-2
```

### Usage

```
go run main.go

manifest descriptor: {application/vnd.oci.image.manifest.v1+json sha256:83ef76b71b0eaba927b1016032e686ef23b61512e93c754ffa86840e1cbef129 419 [] map[] [] <nil> }
Extracted: trivy.db
Extracted: metadata.json
Extraction complete.
manifest descriptor: {application/vnd.oci.image.manifest.v1+json sha256:11a6a12a078ad4c463d1f7dbc53caae56e0c9a5751b77151808140e1df37a018 419 [] map[] [] <nil> }
Extracted: trivy.db
Extracted: metadata.json
Extraction complete.
For Oracle Linux 5 we found 4 edit 0 additions 0 deletions
For Oracle Linux 7 we found 1 edit 0 additions 0 deletions
For Oracle Linux 8 we found 76 edit 0 additions 0 deletions
For alpine 3.12 we found 5 edit 0 additions 0 deletions
For alpine 3.13 we found 3 edit 0 additions 0 deletions
For alpine 3.14 we found 3 edit 0 additions 0 deletions
For alpine 3.15 we found 1 edit 1 additions 0 deletions
For alpine 3.16 we found 4 edit 1 additions 0 deletions
For alpine 3.17 we found 4 edit 1 additions 0 deletions
For alpine 3.18 we found 3 edit 1 additions 0 deletions
For alpine edge we found 4 edit 6 additions 0 deletions
For debian 10 we found 25 edit 0 additions 0 deletions
For debian 11 we found 1 edit 0 additions 0 deletions
For debian 12 we found 1 edit 1 additions 0 deletions
For debian 13 we found 30 edit 0 additions 0 deletions
Vulnerabilities: 612 edit 1 additions 0 deletions
```
